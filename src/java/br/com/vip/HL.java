/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.vip;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Jefferson@jpbx.com.br < https://jpbx.com.br >
 */
@WebServlet(name = "HL", urlPatterns = {"/HL"})
public class HL extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
            
        //String hl=new MD5Factory().md5("123456789:VIP:20181");
        Calendar hj=Calendar.getInstance();
//        hj.set(Calendar.MONTH, 1);
//        hj.add(Calendar.YEAR, 1);
        Calendar behind=Calendar.getInstance();
//        behind.set(Calendar.MONTH, 1);
//        behind.add(Calendar.YEAR, 1);
        Date dia;
        
        hj.add(Calendar.MONTH, 1);
        
        behind.add(Calendar.YEAR, -1);
        
//        out.print(hj.get(Calendar.YEAR)+""+hj.get(Calendar.MONTH)+"<br />");
//        out.print(behind.get(Calendar.YEAR)+""+behind.get(Calendar.MONTH)+"<br />");
        String uuid=new LinuxInfo().hwKey();
        for (Calendar i = behind; i.before(hj); i.add(Calendar.MONTH, 1)) {
            //out.print("VERIFICANDO "+i.get(Calendar.YEAR)+""+i.get(Calendar.MONTH)+"<br />");
            if(request.getParameter("hl").equals(new MD5Factory().md5(uuid+":VIP:"+i.get(Calendar.YEAR)+i.get(Calendar.MONTH)))){
                out.print(true);
                return;
            }
        }
        
//        if(request.getParameter("hl").equals(new MD5Factory().md5(new LinuxInfo().hwKey()+":VIP54321"))){
//            out.print(true);
//            return;
//        }
        out.print(false);
//        response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
//        response.setHeader("Location", "indexx.html");
//        response.setHeader( "Connection", "close" );
//        response.sendRedirect("index.html");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
