/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.vip;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Jefferson@jpbx.com.br < https://jpbx.com.br >
 */
public class LinuxInfo {
    
    public String commandPath(String command){
        String ls_str,ret = command;
        Process ls_proc;
        try {
            ls_proc = Runtime.getRuntime().exec("/usr/bin/which "+command);
            // get its output (your input) stream  
            BufferedReader bf=new BufferedReader(
                    new InputStreamReader(ls_proc.getInputStream()));
            if ((ls_str = bf.readLine()) != null) {
                ret=ls_str;
            }
        } catch (IOException ex) {
            ret=command;
        }
        return ret;
    }
    public String hwKey(){
        Process ls_proc;
        String partition=getPartition();
        try {
//            String comando=commandPath("blkid");
//            System.out.println(comando);
            ls_proc = Runtime.getRuntime().exec("/sbin/blkid");
            BufferedReader bf=new BufferedReader(
                    new InputStreamReader(ls_proc.getInputStream()));
            String ls_str; 
            while ((ls_str = bf.readLine()) != null) {
                if(ls_str.contains(partition))
                    return ls_str.substring(ls_str.indexOf("UUID")).split("\"")[1];
            }
        } catch (IOException ex) {
            System.out.println("DEU RUIM AO PEGAR UUID DA PARTICAO: "+ex.getMessage());
        }
        return "SEM_UUID";
    }
    String getPartition(){ // TRAZ A PARTICAO DO /  EX: /DEV/SDA
        Process ls_proc;
        try {
            ls_proc = Runtime.getRuntime().exec(commandPath("df"));
            BufferedReader bf=new BufferedReader(
                    new InputStreamReader(ls_proc.getInputStream()));
            String ls_str; 
            while ((ls_str = bf.readLine()) != null) {
                String[] line=ls_str.split(" ");
                for(String c:line){
                    if(c.equals("/boot")){ // VMs PRECISEI MUDAR DE / PRA /boot
                        return line[0];
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println("DEU RUIM AO PEGAR PARTICAO DO SISTEMA: "+ex.getMessage());
        }    
        return "NUM_ACHEI";
    }
}
